import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PessoaComponentComponent } from './pessoa-component/pessoa-component.component';
import { CelulaComponentComponent } from './celula-component/celula-component.component';

@NgModule({
  declarations: [
    AppComponent,
    PessoaComponentComponent,
    CelulaComponentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
