# App IgrejaPoa
Aplicativo desenvolvido com [Angular CLI](https://github.com/angular/angular-cli) versão 1.0.3.

## Server de desenvolvimento
Execute `ng serve` para iniciar o servidor de desenvolvimento. A aplicação estará rodando no endereço `http://localhost:4200/`. O aplicativo atualizará automaticamente se qualquer alteração for realizada no código fonte.

## Estrutura de Codigo
Execute `ng generate component component-name` para gerar novos componentes. Você também pode usar `ng generate directive|pipe|service|class|module`.

## Build
Execute `ng build` para buildar o projeto. O artefato do build estará salvo no diretório `dist/`. Use o parametro `-prod` para realizar um build de produção.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).